let collection = [];

// Write the queue functions below.

// Show all elements print
function print (){
	return collection;
}


// Push : add element to top / enqueue
function enqueue (item){
	collection[collection.length]=item;
	return collection;
}
// Pop: remove topmost / Dequeue
function dequeue () {
	let finalCollection =[]
	for (let i = 1; i < collection.length; i++) {
	  finalCollection[finalCollection.length] = collection[i]
	}
	collection = finalCollection
	return collection
}
// Peek : show topmost element / front
function front () {
	return collection[0];
}
// Length: show total number of element / size
function size(){
 		return collection.length;
}

// isEmpty
function isEmpty(){
	if(!collection.length){
		return true
	}
	else {
		return false
	}
}



module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};